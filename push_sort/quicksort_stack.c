/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quicksort_stack.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 21:08:06 by mstygg            #+#    #+#             */
/*   Updated: 2019/09/22 19:17:30 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"


void			sort_stack(t_cont *cont)
{
	if (cont->a_size <= 6)
		start_microsort(cont);
	else
		go_pyramid(cont);
	
}

void			go_pyramid(t_cont *cont)
{
	LL			average;
	int			cur_size;
	t_stack		*tmp;
	
	tmp = cont->a;	
	cur_size = 0;
	while (cont->a_size > 3)
	{
		average = count_average(cont->a, cont->a_size);
		cur_size = cont->a_size;
		while (cur_size > 0)
		{
			if (cont->a->num > average)
			{
				pb(cont);
				cont->b->average_group = average;
			}
			else
				ra(cont);
			--cur_size;
		}
	}
}