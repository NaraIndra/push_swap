/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_stack_utilities.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 19:13:43 by mstygg            #+#    #+#             */
/*   Updated: 2019/09/22 19:15:28 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"


int			count_average(t_stack *a, size_t size)
{
	LL		average;
	t_stack	*tmp;

	average = 0;
	tmp = a->next;
	while (tmp != a)
	{
		average += tmp->num;
		tmp = tmp->next;
	}	
	average += a->num;
	average /= size;	
	return (average);
}