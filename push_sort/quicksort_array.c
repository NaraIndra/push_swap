/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_utilities_quicksort_ar.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 16:53:18 by mstygg            #+#    #+#             */
/*   Updated: 2019/09/22 16:49:10 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void		swap(LL *first, LL *second)
{
	if (first != second)
	{
		*first ^= *second;
		*second ^= *first;
		*first ^= *second;
	}
}

int			partition(LL *ar, int begin, int end)
{
	int		pivot;
	int		i;
	int		j;

	i = begin - 1;
	j = begin;
	pivot = (*(ar + end));
	while (j <= end - 1)
	{
		if (*(ar + j) < pivot)
		{
			++i;
			swap((ar + i),(ar + j));
		}
		++j;
	}
	swap((ar + i + 1), (ar + j));
	return (i + 1);
}

void		quicksort(LL *ar, int begin, int end)
{
	int		p;

	p = 0;
	if (begin >= end)
		return ;
	p = partition(ar, begin, end);
	quicksort(ar, begin, p - 1);
	quicksort(ar, p + 1, end);
}