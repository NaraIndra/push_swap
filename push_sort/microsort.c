/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   microsort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 18:23:48 by mstygg            #+#    #+#             */
/*   Updated: 2019/09/22 19:29:45 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	sort_triple_a(t_cont *cont)
{
	if ((cont->a->num < cont->a->next->num) && (cont->a->next->num < 
	cont->a->next->next->num))
		;
	else if ((cont->a->next->num > cont->a->next->next->num) &&
	(cont->a->next->next->num > cont->a->num))
	{
		sa(cont);
		ra(cont);
	}
	else if ((cont->a->num > cont->a->next->num) && (cont->a->num < 
	cont->a->next->next->num))
		sa(cont);
	else if ((cont->a->num < cont->a->next->num) && (cont->a->num > cont->a->next->next->num))
		rra(cont);
	else if ((cont->a->num > cont->a->next->num) && (cont->a->next->num < cont->a->next->next->num))
		ra(cont);
	else if ((cont->a->num > cont->a->next->num) && (cont->a->next->num > cont->a->next->next->num))
	{
		sa(cont);
		rra(cont);
	}
}

void	sort_double_a(t_cont *cont)
{
	if (cont->a->num < cont->a->next->num)
		;
	else if (cont->a->num > cont->a->next->num)
		sa(cont);
}

void	sort_triple_b(t_cont *cont)
{
	if ((cont->b->num < cont->b->next->num) && (cont->b->next->num < cont->b->next->next->num))
		;
	else if ((cont->b->next->num > cont->b->next->next->num) &&
	(cont->b->next->next->num > cont->b->num))
	{
		sb(cont);
		rb(cont);
	}
	else if ((cont->b->num > cont->b->next->num) && (cont->b->num < cont->b->next->next->num))
		sb(cont);
	else if ((cont->b->num < cont->b->next->num) && (cont->b->num > cont->b->next->next->num))
		rrb(cont);
	else if ((cont->b->num > cont->b->next->num) && (cont->b->next->num < cont->b->next->next->num))
		rb(cont);
	else if ((cont->b->num > cont->b->next->num) && (cont->b->next->num > cont->b->next->next->num))
	{
		sb(cont);
		rb(cont);
	}
}

void	sort_double_b(t_cont *cont)
{
	if (cont->b->num < cont->b->next->num)
		;
	else if (cont->b->num > cont->b->next->num)
		sb(cont);
}

void	start_microsort(t_cont *cont)
{
	if (cont->a_size > 6)
		return ;
	else
		go_pyramid(cont);
	if (cont->a_size == 3)
		sort_triple_a(cont);
	else if (cont->a_size == 2)
		sort_double_a(cont);
	if (cont->b_size == 3)
		sort_triple_b(cont);
	else if (cont->b_size == 2)
		sort_double_b(cont);
}