/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_main.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 16:49:50 by mstygg            #+#    #+#             */
/*   Updated: 2019/09/22 19:16:12 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

int			main(int ac, char **av)
{
	t_cont	*cont;
	char    *line  = (char*)malloc(3 * sizeof(char));

	cont = NULL;
	if (ac == 1)
		return (0);
	cont = create_cont(cont, av);
	check_and_fill_stack_a(cont);
	print_stack(cont);
	sort_stack(cont);
	print_stack(cont);
}