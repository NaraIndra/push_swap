/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 15:28:57 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/22 20:13:16 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "libft_t/includes/libft.h"
#include "ft_printf/prntf/ft_printf.h"


typedef struct		s_stack
{
	int				num;
	int				average_group;
	struct s_stack	*next;
	struct s_stack	*prev;

} 					t_stack;

typedef struct	s_cont
{
	t_stack		*a;
	t_stack		*b;
	size_t		a_size;
	size_t		b_size;
	char 		**argv;
}				t_cont;

#define LL long long

t_cont	*create_cont(t_cont *cont, char **argv);
void	check_and_fill_stack_a(t_cont *cont);
void	check_arg(t_cont *cont);
void	check_duplicates(t_cont *cont);
void	check_max_int_and_fill_stack(t_cont *cont, int i);
void	stack_add_last(t_cont *cont, int num);
void	push_swap_error(t_cont *cont);
void	push_swap_free(t_stack *stack);

/*
**stack_sorting commands
*/

void	sa(t_cont *cont);
void	sb(t_cont *cont);
void    ss(t_cont *cont);
void    ra(t_cont *cont);
void    rb(t_cont *cont);
void    rr(t_cont *cont);
void    rra(t_cont *cont);
void    rrb(t_cont *cont);
void    rrr(t_cont *cont);
void    pa(t_cont *cont);
void    pb(t_cont *cont);
void    stack_add_first(t_cont *cont, int num, int st_a);
void    stack_delete_first(t_cont *cont, int st_a);
void    push_swap_free_cont(t_cont *cont);
void    check_if_sort(t_cont *cont);
int		is_correct_command(char *line);

/*
**stack_sorting functions
*/

int		count_average(t_stack *a, size_t size);
void	go_pyramid(t_cont *cont);
void    sort_stack(t_cont *cont);

void	sort_triple_a(t_cont *cont);
void	sort_double_a(t_cont *cont);
void	sort_triple_b(t_cont *cont);
void	sort_double_b(t_cont *cont);
void	start_microsort(t_cont *cont);
/*
**ar_quick_sort
*/
void		swap(LL *first, LL *second);
int			partition(LL *ar, int begin, int end);
void		quicksort(LL *ar, int begin, int end);

/*
**print_stack_func
*/
void 		print_stack(t_cont *cont);
#endif