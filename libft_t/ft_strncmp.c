/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/02 19:56:57 by mstygg            #+#    #+#             */
/*   Updated: 2018/12/04 16:14:47 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char	c1;
	unsigned char	c2;

	if (n == 0)
		return (0);
	while (n--)
	{
		c1 = *s1++;
		c2 = *s2++;
		if ((unsigned char)c1 != (unsigned char)c2 || !c1 || !c1)
			return ((int)(unsigned char)c1 - (unsigned char)c2);
	}
	return ((int)c1 - c2);
}
