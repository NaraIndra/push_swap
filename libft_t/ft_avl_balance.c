/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avl_balance.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 00:15:49 by mstygg            #+#    #+#             */
/*   Updated: 2018/12/23 23:41:09 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

t_avl_t	*ft_avl_balance(t_avl_t *p)
{
	ft_avl_fix_height(p);
	if (ft_avl_bfactor(p) < -1)
	{
		if (ft_avl_bfactor(p->right) > 0)
		{
			p->right = ft_avl_rot_right(p->right);
		}
		return (ft_avl_rot_left(p));
	}
	if (ft_avl_bfactor(p) > 1)
	{
		if (ft_avl_bfactor(p->left) < 0)
		{
			p->left = ft_avl_rot_left(p->left);
		}
		return (ft_avl_rot_right(p));
	}
	return (p);
}
