/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/02 18:41:36 by mstygg            #+#    #+#             */
/*   Updated: 2018/12/02 19:05:31 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strchr(const char *s, int c)
{
	char ch;

	ch = c;
	if (ch == 0)
		return ((char*)(s + ft_strlen(s)));
	if (!s)
		return (NULL);
	while (*s && *s != ch)
		++s;
	return (*s) ? ((char*)s) : (NULL);
}
