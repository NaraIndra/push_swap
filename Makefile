NAME = push_swap
CFLAGS = -Wall -Wextra


all: $(NAME)

$(NAME):
	make -C checker/
	make -C push_sort/

cleanlib:
	make -C libft_t/ fclean

cleanprintf:
	make -C ft_printf/ fclean

cleanchecker:
	make -C checker/ fclean

cleanutil:
	make -C ps_utilities/ fclean

cleansort:
	make -C push_sort/ fclean


fclean: cleanchecker cleanlib cleanprintf cleansort cleanutil
	/bin/rm -rf $(NAME) 

re: fclean all
