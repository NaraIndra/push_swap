/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_if_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 19:30:04 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/22 17:49:15 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void    check_if_sort(t_cont *cont)
{
	t_stack *tmp;
	int     is_sort;

	tmp = cont->a;
	is_sort = (cont->b) ? 0 : 1;
	while (tmp->next != cont->a)
	{
		if (tmp->num > tmp->next->num)
			is_sort = 0;
		tmp = tmp->next;
	}
	if (is_sort)
		write(1, "OK\n", 3);
	else
		write(1, "KO\n", 3);
}
