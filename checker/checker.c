/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 15:39:17 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/22 18:50:38 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

int     is_correct_command(char *line)
{
	return (!ft_strncmp(line, "sa", 2) || !ft_strncmp(line, "sb", 2) ||
			!ft_strncmp(line, "ss", 2) || !ft_strncmp(line, "pa", 2) || !ft_strncmp(line, "pb", 2)
			|| !ft_strncmp(line, "ra", 2) || !ft_strncmp(line, "rb", 2) || !ft_strncmp(line, "rr", 2)
			|| !ft_strncmp(line, "rra", 2) || !ft_strncmp(line, "rrb", 2) || !ft_strncmp(line, "rrr", 2));
}

void    do_command(t_cont *cont, char *line)
{
	if (ft_strncmp(line,"sa", 2))
		sa(cont);
	else if (ft_strncmp(line,"sb", 2))
		sb(cont);
	else if (ft_strncmp(line,"ss", 2))
		ss(cont);
	else if (ft_strncmp(line,"pa", 2))
		pa(cont);
	else if (ft_strncmp(line,"pb", 2))
		pb(cont);
	else if (ft_strncmp(line,"ra", 2))
		ra(cont);
	else if (ft_strncmp(line,"rb", 2))
		rb(cont);
	else if (ft_strncmp(line,"rr", 2))
		rr(cont);
	else if (ft_strncmp(line,"rra", 2))
		rra(cont);
	else if (ft_strncmp(line,"rrb", 2))
		rrb(cont);
	else if (ft_strncmp(line,"rrr", 2))
		rrr(cont);
}

static	size_t count_stack_size(t_stack *stack)
{
	t_stack 	*tmp;
	size_t		size;

	size = (stack != NULL) ? (1) : (0);
	tmp = stack->next;
	while (tmp != stack)
	{
		++size;
		tmp = tmp->next;
	}

	return (size);
}

int 	main (int argc, char **argv)
{
	t_cont	*cont;
	char    *line = (char*)malloc(3 * sizeof(char));

	cont = NULL;
	if (argc == 1)
	{
		return (0);
	}	
	cont = create_cont(cont, argv);
	check_and_fill_stack_a(cont);
	while (read(0, line, 3) > 0)
	{
		if (!(is_correct_command(line)))
		{
			ft_strdel(&line);
			push_swap_error(cont);
		}
		else
		{
			do_command(cont, line);
			// ft_strdel(&line);
		}
	}
	//
	// LL average = count_average(cont->a, cont->a_size);
	print_stack(cont);
	// quicksort_stack(cont);
	// print_stack(cont);
	// check_if_sort(cont);
	// push_swap_free_cont(cont);
	return (0);
}
