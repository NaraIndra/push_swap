/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pa_pb.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 19:33:01 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/22 17:51:03 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void    pa(t_cont *cont)
{
	long int    i;

	if (cont->b)
	{
		i = cont->b->num;
		stack_delete_first(cont, 0);
		stack_add_first(cont, i, 1);
	}
	--(cont->b_size);
	++(cont->a_size);
}

void    pb(t_cont *cont)
{
	long int    i;

	if (cont->a)
	{
		i = cont->a->num;
		stack_delete_first(cont, 1);
		stack_add_first(cont, i, 0);
	}
	--(cont->a_size);
	++(cont->b_size);
}
