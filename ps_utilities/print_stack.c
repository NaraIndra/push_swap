/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 16:21:03 by mstygg            #+#    #+#             */
/*   Updated: 2019/09/21 19:37:13 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void 		print_stack(t_cont *cont)
{
	size_t	max_size;
	t_stack	*tmp_a;
	t_stack	*tmp_b;
	
	max_size = ((cont->a_size >= cont->b_size) ? (cont->a_size) : (cont->b_size));
	tmp_a = cont->a;
	tmp_b = cont->b;
	while (max_size > 0)
	{
		if (max_size <= cont->a_size)
		{
			ft_printf("%d", tmp_a->num);
			tmp_a = tmp_a->next;

		}
		else
			
		
		if (max_size <= cont->b_size)
		{
			ft_printf("%10d, cluster:%d", tmp_b->num, tmp_b->average_group);
			tmp_b = tmp_b->next;
		}
		ft_putchar('\n');
		--max_size;
	}

}      