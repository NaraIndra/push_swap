/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_error.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 21:00:35 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/21 19:36:28 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	push_swap_free(t_stack *stack)
{
	t_stack	*next;
	t_stack	*current;

	if (stack)
	{
		next = stack->next;
		while (next != stack)
		{
			current = next;
			next = next->next;
			free(current);
		}
		free(next);
	}
}

void    push_swap_free_cont(t_cont *cont)
{
	if (cont)
	{
		if (cont->a)
			push_swap_free(cont->a);
		if (cont->b)
			push_swap_free(cont->b);
		free(cont);
	}
}

void	push_swap_error(t_cont *cont)
{
	write(1, "Error\n", 6);
	if (cont)
	{
		if (cont->a)
			push_swap_free(cont->a);
	}
	exit(-1);
}
