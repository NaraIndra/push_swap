/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rra_rrb_rrr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 19:34:29 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/21 19:37:03 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void    rra(t_cont *cont)
{
	if (cont->a)
		cont->a = cont->a->prev;
}

void    rrb(t_cont *cont)
{
	if (cont->b)
		cont->b = cont->b->prev;
}

void    rrr(t_cont *cont)
{
	rra(cont);
	rrb(cont);
}
