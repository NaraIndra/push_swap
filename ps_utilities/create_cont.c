/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_cont.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mjon-hol <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 15:43:13 by mjon-hol          #+#    #+#             */
//*   Updated: 2019/09/14 21:11:40 by mjon-hol         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

t_cont	*create_cont(t_cont *cont, char **argv)
{
	if (!(cont = (t_cont *)malloc(sizeof(t_cont))))
		push_swap_error(cont);
	cont->a = NULL;
	cont->b = NULL;
	cont->a_size = 0;
	cont->b_size = 0;
	cont->argv = argv;
	return (cont);
}

void	check_arg(t_cont *cont)
{
	int 	i;
	int		j;
	char 	*str;

	i = 1;
	str = cont->argv[i];
	while (str)
	{
		j = 0;
		while (str[j] != '\0')
		{
			if (str[j] == ' ' || (str[j] >= '0' && str[j] <= '9'))
				j++;
			else if ((str[j] == '+' || str[j] == '-') && (str[j + 1] >= '0'
			&& str[j + 1] <= '9') && (j == 0 || str[j - 1] == ' '))
				j++;
			else
				push_swap_error(cont);
		}
		str = cont->argv[++i];
	}
}

void	check_duplicates(t_cont *cont)
{
	t_stack	*first;
	t_stack *second;

	first = cont->a;
	while (first != cont->a->prev)
	{
		second = first->next;
		while (second != cont->a)
		{
			if (first->num == second->num)
				push_swap_error(cont);
			second = second->next;
		}
		first = first->next;
	}
}

static size_t	count_num_size(long long k)
{
	int size;

	size = 0;
	
	while (k > 0)
	{
		k /= 10;
		++size;
	}
	return ((size > 0) ? (size) : (1));
}

void	check_max_int_and_fill_stack(t_cont *cont, int i)
{
	char		*str;
	long long	k;


	str = cont->argv[i];
	while (str)
	{
		while (*str != '\0')
		{
			while (*str != '\0' && *str == ' ')
				++str;
			while (*str != '\0' && (*str == '+' || *str == '-'))
				++str;
				/*
				**k здесь не может превысить макс и мин. нужно переделать atoi на atol
				**(соответствующая функция есть в библиотеке printf)
				*/
			k = ft_atol((const U_CHAR *)str);
			if (k < -2147483648 || k > 2147483647)
				push_swap_error(cont);
			stack_add_last(cont, k);
			str += count_num_size(k);
		}
		str = cont->argv[++i];
	}
	if (!cont->a)
		push_swap_error(cont);
}

void	check_and_fill_stack_a(t_cont *cont)
{
	int i;

	i = 1;
	check_arg(cont);
	check_max_int_and_fill_stack(cont, i);
	check_duplicates(cont); //сначала заполняем стек, потом проверяем его на дубли
}