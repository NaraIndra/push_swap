/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ra_rb_rr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 19:34:04 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/21 19:37:08 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void    ra(t_cont *cont)
{
	if (cont->a)
		cont->a = cont->a->next;
}

void    rb(t_cont *cont)
{
	if (cont->b)
		cont->b = cont->b->next;
}

void    rr(t_cont *cont)
{
	ra(cont);
	rb(cont);
}
