/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_stack.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 20:45:43 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/21 19:36:53 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	stack_add_last(t_cont *cont, int num)
{
	t_stack	**stack_a;
	t_stack	*stack_new;

	stack_a = &cont->a;
	if (*stack_a)
	{
		if (!(stack_new = (t_stack *)malloc(sizeof(t_stack))))
			push_swap_error(cont);
		stack_new->next = *stack_a;
		stack_new->prev = (*stack_a)->prev;
		(*stack_a)->prev = stack_new;
		stack_new->num = num;
		stack_new->prev->next = stack_new;
	}
	else
	{
		if (!(*stack_a = (t_stack *)malloc(sizeof(t_stack))))
			push_swap_error(cont);
		(*stack_a)->num = num;
		(*stack_a)->next = *stack_a;
		(*stack_a)->prev = *stack_a;
	}
	++(cont->a_size);
}

void    stack_add_first(t_cont *cont, int num, int st_a)
{
	t_stack	**stack_a_b;
	t_stack	*stack_new;

	stack_a_b = (st_a == 1) ? &cont->a : &cont->b;
	if (*stack_a_b)
	{
		if (!(stack_new = (t_stack *)malloc(sizeof(t_stack))))
			push_swap_error(cont);
		stack_new->next = *stack_a_b;
		stack_new->prev = (*stack_a_b)->prev;
		(*stack_a_b)->prev = stack_new;
		stack_new->num = num;
		stack_new->prev->next = stack_new;
		*stack_a_b = (*stack_a_b)->prev;
	}
	else
	{
		if (!(*stack_a_b = (t_stack *)malloc(sizeof(t_stack))))
			push_swap_error(cont);
		(*stack_a_b)->num = num;
		(*stack_a_b)->next = *stack_a_b;
		(*stack_a_b)->prev = *stack_a_b;
	}
}

void    stack_delete_first(t_cont *cont, int st_a)
{
	t_stack	**stack_a_b;
	t_stack	*stack_new;

	stack_a_b = (st_a == 1) ? &cont->a : &cont->b;
	if (*stack_a_b)
	{
		if ((*stack_a_b)->next == *stack_a_b)
		{
			free(*stack_a_b);
			*stack_a_b = NULL;
		}
		else
		{
			stack_new = *stack_a_b;
			*stack_a_b = (*stack_a_b)->next;
			stack_new->prev->next = *stack_a_b;
			(*stack_a_b)->prev = stack_new->prev;
			free(stack_new);
		}
	}
}
