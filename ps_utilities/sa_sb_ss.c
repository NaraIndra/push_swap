/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sa_sb_ss.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 21:17:47 by mjon-hol          #+#    #+#             */
/*   Updated: 2019/09/21 19:36:57 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	sa(t_cont *cont)
{
	long int	i;

	if (cont->a && cont->a->next != cont->a)
	{
		i = cont->a->num;
		cont->a->num = cont->a->next->num;
		cont->a->next->num = i;
	}
}

void	sb(t_cont *cont)
{
	long int	i;

	if (cont->b && cont->b->next != cont->b)
	{
		i = cont->b->num;
		cont->b->num = cont->b->next->num;
		cont->b->next->num = i;
	}
}

void    ss(t_cont *cont)
{
	sa(cont);
	sb(cont);
}
